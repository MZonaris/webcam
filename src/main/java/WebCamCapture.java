import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WebCamCapture extends Applet implements ActionListener{

    private Dimension dimension = WebcamResolution.QVGA.getSize();
    private Webcam webcam = null;
    private WebcamPanel webcamPanel = null;
    private JButton on;
    private JButton off;

    public void start() {
        super.start();

        webcam = Webcam.getDefault();
        webcam.setViewSize(dimension);

        webcamPanel = new WebcamPanel(webcam, false);
        webcamPanel.setFPSDisplayed(true);

        on = new JButton("on");
        off = new JButton("off");

        add(webcamPanel);
        add(on);
        add(off);

        on.addActionListener(this);
        off.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        JButton source = (JButton) e.getSource();

        if(source.getLabel().equals("on")){
            if(webcam.isOpen()){
                webcam.close();
            }

            int i = 0;
            do {
                if (webcam.getLock().isLocked()) {
                    System.out.println("Waiting for lock to be released " + i);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e1) {
                        return;
                    }
                } else {
                    break;
                }
            } while (i++ < 3);

            webcam.open();
            webcamPanel.start();
        } else{
            if(source.getLabel().equals("off")){
                webcam.close();
                webcamPanel.stop();
            }
        }
    }


        /*if(webcam.isOpen()){
            webcam.close();
        }

        int i = 0;
        do {
            if (webcam.getLock().isLocked()) {
                System.out.println("Waiting for lock to be released " + i);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e1) {
                    return;
                }
            } else {
                break;
            }
        } while (i++ < 3);

        webcam.open();
        webcamPanel.start();
    }*/




}